//
//  Missile.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 06/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Missile_hpp
#define Missile_hpp

#include <GLUT/glut.h>
#include "Entity.hpp"

class Missile : public Entity {
    
public:
    Missile(float x, float y, float dir);
    void update();
    void draw();
    float ydir;
    
};

#endif /* Missile_hpp */

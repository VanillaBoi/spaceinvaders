//
//  Block.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 13/04/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Block_hpp
#define Block_hpp

#include "Entity.hpp"
#include <GLUT/glut.h>

class Block : public Entity
{
public:
    Block(float xposition);
    void draw();
    
};

#endif /* Block_hpp */

//
//  Missile.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 06/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#include "Missile.hpp"

Missile::Missile(float x, float y, float dir)
{
    xpos=x;
    ypos=y;
    ydir=dir;
    alive=1;
}

void Missile::update()
{
    ypos+=ydir;
}

void Missile::draw()
{
    glLoadIdentity();
    glTranslatef(xpos, ypos, 0);
    glScalef(0.02, 0.02, 0.02);
    glutWireIcosahedron();
}

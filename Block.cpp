//
//  Block.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 13/04/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#include "Block.hpp"

Block::Block(float xposition)
{
    xpos=xposition;
    ypos=-0.6;
    alive=3;
}


void Block::draw()
{
    glColor3f(1.0, 1.0, 1.0);
    glLoadIdentity();
    
    glTranslatef(xpos, ypos, 0);
    
    glTranslatef(-0.07, 0, 0);
    glutSolidCube(0.05);
    glTranslatef(0.15, 0, 0);
    glutSolidCube(0.05);
    
    glTranslatef(-0.15, 0.05, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
    
    glTranslatef(-0.15, 0.05, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
    glTranslatef(0.05, 0, 0);
    glutSolidCube(0.05);
}

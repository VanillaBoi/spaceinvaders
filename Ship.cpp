//
//  Ship.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 05/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#include "Ship.hpp"

Ship::Ship()
{
    alive=3;
    ypos=-0.9;
}


void Ship::draw()
{
        glLoadIdentity();
    
        glTranslatef(xpos, -0.9, 0);
    
        glScalef(0.75, 0.75, 0.0);
    
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
    
        glTranslatef(-0.15, 0.05, 0);
    
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
        glTranslatef(0.05, 0, 0);
        glutSolidCube(0.05);
        
        glTranslatef(-0.05, 0.05, 0);
        glTranslatef(-0.025, 0.0, 0);
        glutSolidCube(0.05);
    
    

}

void Ship::update()
{
    
    xpos = Input::x;

}

void Ship::shoot()
{
    if(glutGet(GLUT_ELAPSED_TIME)>=shootTs+500){
    missiles.push_back(new Missile(Input::x+0.075, -0.75, 0.1));
    shootTs=glutGet(GLUT_ELAPSED_TIME);
    }
}

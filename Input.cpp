//
//  Input.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 06/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#include "Input.hpp"

void Input::specialKeys( int key, int x, int y) {
    
    
    //  Right arrow - increase rotation by 5 degree
    if (key == GLUT_KEY_RIGHT)
            rightKey=true;
    
    
    //  Left arrow - decrease rotation by 5 degree
    else if (key == GLUT_KEY_LEFT)
            leftKey=true;
    
}

void Input::specialUpFunc( int key, int x, int y) {
    
    
    //  Right arrow - increase rotation by 5 degree
    if (key == GLUT_KEY_RIGHT)
            rightKey=false;
    
    
    //  Left arrow - decrease rotation by 5 degree
    else if (key == GLUT_KEY_LEFT)
           leftKey=false;
    
    
    
    else if (key == GLUT_KEY_UP)
            upKey=false;
    
    
    else if (key == GLUT_KEY_DOWN)
            downKey=false;
    
    
}

void Input::KeyboardDown(unsigned char key, int x, int y)
{

    if (key == 32)//ASCII code for space
        Input::sbKey=true;

}


void Input::KeyboardUp(unsigned char key, int x, int y)
{
    if (key == 32)//ASCII code for space
         Input::sbKey=false;
}

//
//  Ship.hpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 05/02/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//

#ifndef Ship_hpp
#define Ship_hpp


#include <GLUT/glut.h>
#include "Input.hpp"
#include "Missile.hpp"
#include "Entity.hpp"
#include <vector>


class Ship: public Entity {
    
public:
    
   
    Ship();
    void draw();
    void update();
    void shoot();
    std::vector  <Missile*> missiles;
    float shootTs;

    
};

#endif /* Ship_hpp */

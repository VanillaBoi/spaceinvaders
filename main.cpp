//
//  main.cpp
//  SpaceInvaders
//
//  Created by Marlon Youngerman on 29/01/2019.
//  Copyright © 2019 Marlon Youngerman. All rights reserved.
//


#include <GLUT/glut.h>
#include <vector>
#include "Ship.hpp"
#include "Input.hpp"
#include "Alien.hpp"
#include "Missile.hpp"
#include "Block.hpp"
#include <stdlib.h>
#include <iostream>
#include <cmath>


double rotate_y=0;
double rotate_x=0;
float trans_x=0;
float Input::x=0;
float Input::rotate=0;
bool Input::sbKey=false;
bool Input::leftKey=0;
bool Input::rightKey=0;
bool Input::upKey=0;
bool Input::downKey=0;
float Alien::dir=0.04;
int timeStamp=0;
int timeStamp1=0;
int missileTimeStamp=0;


std::vector <Entity*> missiles;
std::vector <Entity*> aliens;
std::vector <Entity*> entities;
Ship *s = new Ship();

void collision()
{
    for(int i=0; i<entities.size(); i++)
    {
        for(int ii=0; ii<missiles.size(); ii++)
        {
            if( std::sqrt(std::pow(missiles[ii]->xpos-entities[i]->xpos,2) + std::pow(missiles[ii]->ypos-entities[i]->ypos,2 )) < 0.1 && missiles[ii]!=entities[i])
            {
                entities[i]->alive--;
                missiles[ii]->alive=0;
            }
        }
    }
    
    for(int i=0; i<missiles.size(); i++)
    {
        if(missiles[i]->alive==0)
        missiles[i] = nullptr;
       
    }
    
    for(int i=0; i<entities.size(); i++)
    {
        if(entities[i]->alive==0)
        entities[i] = nullptr;
    }
    
    for(int i=0; i<aliens.size(); i++)
    {
        if(aliens[i]->alive==0)
            aliens[i] = nullptr;
    }
    
    entities.erase(std::remove(entities.begin(), entities.end(), nullptr), entities.end());
    missiles.erase(std::remove(missiles.begin(), missiles.end(), nullptr), missiles.end());
    aliens.erase(std::remove(aliens.begin(), aliens.end(), nullptr), aliens.end());
}

void HpBar()
{
    glLoadIdentity();
    if(s->alive>0)
    {
        glTranslatef(-0.95, 0.95, 0);
        glutSolidCube(0.05);
    }
    if(s->alive>1)
    {
        glTranslatef(0.07, 0.0, 0);
        glutSolidCube(0.05);
    }
    if(s->alive>2)
    {
        glTranslatef(0.07, 0.0, 0);
        glutSolidCube(0.05);
    }
}


void specialKeys( int key, int x, int y ) {
    
    
    //  Right arrow - increase rotation by 5 degree
    if (key == GLUT_KEY_RIGHT)
        trans_x += 0.03;

    
    //  Left arrow - decrease rotation by 5 degree
    else if (key == GLUT_KEY_LEFT)
        trans_x -= 0.03;

    
    
    else if (key == GLUT_KEY_UP)
        rotate_y += 0.5;
    
    else if (key == GLUT_KEY_DOWN)
        rotate_y -= 0.5;
}



void displayCallback()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glutPostRedisplay();
}

void draw()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    for(int i=0;i<entities.size();i++)
    {
        entities[i]->draw();
    }
    HpBar();
    
    
    glFlush();
    glutPostRedisplay();
}


void update()
{
    if( timeStamp1+17 < glutGet(GLUT_ELAPSED_TIME) )
    {
    
        timeStamp1=glutGet(GLUT_ELAPSED_TIME);
        
        bool b=false;
        
        collision();
    
        if (Input::rightKey)
            Input::x+=0.01;
    
        if (Input::leftKey)
            Input::x-=0.01;
    
        if (Input::sbKey)//SpaceBar
        {
            if(glutGet(GLUT_ELAPSED_TIME)>=missileTimeStamp+500)
            {
                missiles.push_back(new Missile(Input::x+0.055, -0.85, 0.05));
                entities.push_back(missiles.back());
                missileTimeStamp=glutGet(GLUT_ELAPSED_TIME);
            }
        }
        
        for(int i=0;i<entities.size();i++)
        {
             entities[i]->update();
        }
        
        float lastpos=2.0;
        //this loop is here to find the y position of the last row of aliens
        for(int i=0;i<aliens.size();i++)
        {
            if(aliens[i]->ypos<lastpos)
                {
                    lastpos=aliens[i]->ypos;
                    std::cout<<lastpos<<std::endl;
                }
        }
        
        if( ( rand() % 11) == 10)
            {
                if (aliens.size()>0)
                {
                    int i = rand() % aliens.size();
                    if(aliens[i]->ypos==lastpos)
                    {
                        missiles.push_back(new Missile(aliens[i]->xpos-0.05, aliens[i]->ypos-0.12, -0.01));
                        entities.push_back(missiles.back());
                    }
                }
            }
       
    
        for(int i=0;i<aliens.size();i++)
        {
            if((aliens[i]->xpos>=0.9 || aliens[i]->xpos<=-0.9))
            {
                b=true;
                break;
            }
        }
        if (b)
        {
            Alien::dir=(-Alien::dir);
            for(int a=0;a<aliens.size();a++)
            {
                aliens[a]->xpos+=Alien::dir;
                aliens[a]->ypos-=0.05;
            }
        }
    

            draw();

    }
}


int main(int argc, char *argv[])
{
    int win;
   
    
    
    glutInit(&argc, argv);        /* initialize GLUT system */
    
    glutInitDisplayMode(GLUT_RGB);
    glutInitWindowSize(1024,768);
    win = glutCreateWindow("Space Invaders");// create window
    
    srand(glutGet(GLUT_ELAPSED_TIME));//seed the rng
    
    
    float x=-0.75;
    float y=0.9;
    for(int i=0;i<40;i++)
    {
         x+=0.2;
        
        aliens.push_back(new Alien(x,y));
        entities.push_back(aliens[i]);
        
        
         if(x>=0.75)
         {
             x=-0.75;
             y-=0.2;
         }
    }
    
    entities.push_back(s);
    

    entities.push_back(new Block(-0.6));
    entities.push_back(new Block(0));
    entities.push_back(new Block(0.6));
    
    
    
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0,0.0,0.0,0.0);    /* set background to black */
    glutSpecialFunc(Input::specialKeys);
    glutSpecialUpFunc(Input::specialUpFunc);
    glutKeyboardFunc(Input::KeyboardDown);
    glutKeyboardUpFunc(Input::KeyboardUp);
    glutIdleFunc(update);
    
    glutDisplayFunc(displayCallback);
    
    glutMainLoop();  
    
    return 0;
}
